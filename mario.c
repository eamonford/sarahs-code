#include <cs50.h>
#include <stdio.h>

// prototype
int GetHeight(void);

int main(void)
{
    int height = GetHeight();
    for (int row = 0; row < height; row ++)
    {
        //print spaces:
        for (int i = 0; i < height - row - 1; i++)
        {
            printf (" ");
        }
        //print hashes:
        for (int j = 0; j < row+2; j ++)
        {
            printf ("#");
        }
        printf ("\n");
    }
}    

int GetHeight(void)
{
    int height;
    do
    {
        printf("height:");
        height = GetInt();
    }
    while (height < 0 || height > 23);
    return height;
}