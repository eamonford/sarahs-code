#include <stdio.h>
#include <cs50.h>
#include <math.h>

//prototype
int getCent(void);
float getDollar(void);
int getCoin(void);

int main(void)
{
    int coinCount = getCoin();
    printf ("%i\n", coinCount);
}

int getCoin(void)
{
    int cent = getCent();
    int coinCount = 0;
    while (cent >= 25)
    {
        coinCount ++;
        cent = cent - 25;
    }
    while (cent >= 10)
    {
        coinCount ++;
        cent = cent - 10;
    }
    while (cent >= 5)
    {
        coinCount ++;
        cent = cent - 5;
    }
    while (cent >= 1)
    {
        coinCount ++;
        cent = cent - 1;
    }
    return coinCount;
}

int getCent(void)
{
    float dollar = getDollar();
    int cent = round(dollar*100);
    return cent;
}

float getDollar(void)
{
    float dollar;
    do
    {
        printf("How much change is owed?\n");
        dollar = GetFloat();
    }
    while (dollar < 0);
    return dollar;
}