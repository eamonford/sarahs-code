#include <cs50.h>
#include <stdio.h>

int main(void)
{
    printf("How long is your shower time?\n");
    int min = GetInt();
    printf("That is %i bottles of water!\n", min*12);
}
